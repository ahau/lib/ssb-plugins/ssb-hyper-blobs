// run this file: DEBUG=artefact-* node example.js // rm

/* setup */
// const caps = require('ssb-caps') // uncomment
const caps = { // rm
  shs: require('crypto').randomBytes(32).toString('base64') // rm
} // rm
const Config = require('ssb-config/inject')

const stack = require('secret-stack')()
  .use(require('ssb-db'))
  // .use(require('ssb-hyper-blobs')) // uncomment
  .use(require('../')) // rm

const config = Config('temp', {
  path: '/tmp/ssb-hyper-blobs' + Date.now(),
  caps
})

const ssb = stack(config)

/* adding a file */
const path = require('path')
const pull = require('pull-stream')
const file = require('pull-file')

pull(
  file(path.resolve(__dirname, '../README.md')),
  ssb.hyperBlobs.add((err, data) => {
    console.log(err, data)
    // {
    //   driveAddress: 'e8b2557f90b94f559abae254802594034993f9a93da55e5f147e2e870d8f7955',
    //   blobId: '2df0cb34-77e5-40af-99ad-52d85ff67d35',
    //   readKey: '59e9eeb226bd77581d849346b2063eebac518e06e5f3637b75a15e68ce37eaab'
    // }

    httpGet(data, 'README.md') // rm
  })
)

/* reading a file over http */
const axios = require('axios')

function httpGet (data, fileName) { // rm
  // block-<
  const { driveAddress, blobId, readKey } = data
  const url = `http://localhost:26836/drive/${driveAddress}/blob/${blobId}?readKey=${readKey}&fileName=${fileName}`
  // if this was e.g. an image you can use this URL directly in an <img> tag
  // NOTE: the query.fileName is used to pin mimeType which is helpful for some html rendering e.g <video>

  axios.get(url)
    .then(res => {
      console.log(res.data.slice(0, 600), '...')

      // ...

      ssb.close() // rm
    })
  // block-<
} // rm
