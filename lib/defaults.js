const SEC = 1000
const MIN = 60 * SEC
const HOUR = 60 * MIN
const GB = 1024 * 1024 * 1024

module.exports = {
  autoPrune: {
    startDelay: 10 * MIN,
    intervalTime: HOUR,
    maxRemoteSize: 5 * GB
  }
}
