const test = require('tape')
const Server = require('./testbot')

// probably un-needed
test.skip('init + shutdown', t => {
  const server = Server()

  t.deepEqual(
    Object.keys(server.hyperBlobs),
    ['onReady', 'add', 'driveAddress', 'registerDrive', 'prune'],
    'has hyperBlobs methods'
  )

  server.hyperBlobs.driveAddress((_, address) => {
    t.true(address.length > 0, 'has driveAddress')

    server.close(err => {
      t.error(err, 'shuts down well')
      t.end()
    })
  })
})
